# Annuaire C

Annuaire partagé en C avec un modèle client serveur en réseau réaliser avec le language C standard gnu99

[![version](https://img.shields.io/badge/Version-2.0.0-green.svg)](https://gitlab.com/babson4/annuaire_l3/-/tags/2.0.0)
[![note](https://badgen.net/badge/icon/%3F%2F20?label=Note)](https://www.upssitech.eu/)
[![Build Status](https://gitlab.com/babson4/annuaire_l3/badges/master/pipeline.svg)](https://gitlab.com/babson4/annuaire_l3/-/commits/master)

## Auteurs
👤 **B. SIBOUT**

- GitLab: [@babson4](https://gitlab.com/babson4)

👤 **E. HOUNZANGBE**

👤 **T. VIVIER**

👤 **T. LOPEZ**

## Installation et dépendances

Ce projet utilise:
* [make](https://linux.die.net/man/1/make) (>= 4.3)
* [gcc](https://gcc.gnu.org/) (>= 10.2.0)
* [libssl](https://packages.debian.org/fr/jessie/libssl-dev)
* optionnel [oclint](https://docs.oclint.org/en/stable/) (>= 20.11)
* optionnel [valgrind](http://valgrind.org) (>= 3.16.1)

Installation via make: 
```bash
#Creation du fichier dans le home directory par simplicité, il peut être déplacé
mkdir ~/annuaire_l3
git clone https://gitlab.com/babson4/annuaire_l3 ~/annuaire_l3/
cd ~/annuaire_l3/

#Installation des dépendances fortes
make install

#Installation des dépendances optionnels
make install/optional
```

Installation manuel:
```bash
#Creation du fichier dans le home directory par simplicité, il peut être déplacé
mkdir ~/annuaire_l3
git clone https://gitlab.com/babson4/annuaire_l3 ~/annuaire_l3/
cd ~/annuaire_l3/

#Installation des dépendances fortes
sudo sudo apt install libssl-dev libpcap-dev

# Installation des dépendances optionnels
# oclint 20.11
wget -q -O /opt/oclint.tar.gz https://github.com/oclint/oclint/releases/download/v20.11/oclint-20.11-llvm-11.0.0-x86_64-linux-ubuntu-20.04.tar.gz
tar xzvf /opt/oclint.tar.gz -C /opt/
export PATH=/opt/oclint-20.11/bin:$PATH

# Valgrind
sudo apt install valgrind
```

## 🚀 Utilisation

```bash
cd ~/annuaire_l3
make help
make
bin/serverMain
```

Pour généré le rapport Oclint :
`make oclint/(client/server)` le rapport sera dans doc/

## ⭐️ FAQ

Lors de l'execution de `make valgrind/server` j'ai un message d'erreur me disant `bash: valgrind : commande introuvable`
>  `apt install valgrind`

## 📝 License

Copyright © 2020-2021 B. SIBOUT && E. HOUNZANGBE && T. LOPEZ && T. VIVIER

This project is MIT licensed.

<img src="https://stri-online.net/FTLV/pluginfile.php/1/theme_adaptable/adaptablemarketingimages/0/Logos.png" width="50%"/>
