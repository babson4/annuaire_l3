# **Service d’annuaires partagés**

par _Bastien SIBOUT, Emmanuel HOUNZANGBE, Tristan LOPEZ et Tristan VIVIER_

Version : 2.0.0

# Le Résumé

_Le présent document explique le fonctionnement de notre projet concernant le service d’annuaires partagés. Il comporte une partie sur les PDU (Protocol Data Unit ou Unité de données de Protocole), une partie concernant la connexion et la déconnexion du client au serveur, une partie expliquant les échanges avec un compte utilisateur standard et une partie avec un utilisateur administrateur. Dans chaque partie, on présentera les types de requêtes et réponses, on expliquera les codes d’erreurs et il y aura des exemples d’échange entre le client et le serveur._

# _Table des matières_


- [1. Introduction](#introduction)
- [2. Spécifications du protocole d'échange](#pdu)
- [3. Connexion/Déconnexion](#co-deco)
  - [3.1. Connexion](#connexion)
  - [3.2. Déconnexion](#deco)
- [4. Utilisateur](#utilisateur)
  - [4.1. Afficher son annuaire](#afficher_annuaire)
  - [4.2. Consulter un autre annuaire](#consulter_autre_annuaire)
  - [4.3. Rechercher dans son annuaire](#search)
  - [4.4. Ajouter un contact](#add_contact)
  - [4.5. Modification d'une ligne de l'annuaire](#modif_ligne)
  - [4.6. Suppression d'une ligne](#del_ligne)
  - [4.7. Afficher la liste des personnes qui ont des droits de lecture sur un annuaire](#afficher_liste_personne_droit)
  - [4.8. Autoriser une personne à consulter mon annuaire](#autoriser_annuaire)
- [5. Partie administrateur](#admin)
  - [5.10. Créer un utilisateur](#add_user)
  - [5.11. Supprimer un utilisateur](#del_user)
  - [5.12. Afficher tous les utilisateurs](#all_user)


# 1. Introduction <a name="introduction"></a>

Le présent document explique le fonctionnement d’annuaires partagés programmé en C. Cet annuaire se base sur un échange entre un client et un serveur. Le principe de cette application est d’offrir aux utilisateurs la possibilité de consulter leurs annuaires comportant le nom, le prénom, l’adresse email ainsi que le numéro de téléphone de leurs contacts. Chaque utilisateur pourra rajouter ou supprimer des contacts selon son bon vouloir. Pour réguler les comptes utilisateurs, un administrateur pourra rajouter ou supprimer des utilisateurs standards et aura tous les droits sur les annuaires. Pour permettre le bon fonctionnement de l’application, nous avons mis en en place un protocole d’échange un client et un serveur.

Vous pouvez retrouver le projet sur ce lien gitlab : https://gitlab.com/babson4/annuaire_l3

<br>
<br>
<br>



# 2. Spécifications du protocole d'échange <a name="pdu"></a> <a name="code_erreur"></a>

Pour la réalisation de ce projet, il était nécessaire de faire intervenir le principe de PDU.
Les messages échangés entre le client et le serveur comportent différents champs qui varient en nombre et en type, en fonction du type de requête. Chaque champ est séparé par un autre **d'un espace**

    Exemple:
    <champ#1> <champ#2> <champ#3> <champ#X>

_Le premier champ détermine le type de requête_

On distingue 8 type de requêtes, réprésentant les échanges Client <-> Serveur. Elles seront détaillées ci-après:

-   AUTH
-   ADD
-   DELETE
-   GET
-   GRANT
-   EDIT
-   SEARCH
-   QUIT

De même on trouve 8 types de codes d'erreur, détaillés ci-dessous :

- 200 : La requête a été bien traitée.
- 400 : Le mot de passe et le nom d'utilisateur ne correspondent pas.
- 401 : Erreur lors de l'émission du côté du client.
- 402 : Format de la requête non valide qui découle d'un mauvais choix ou d'une mauvaise saisie de l'utilisateur.
- 403 : Autre erreur indéfinie côté client.
- 501 : Erreur lors de l'émission du côté du serveur.
- 502 : Erreur lors de l'ouverture d'un fichier sur le serveur.
- 503 : Le serveur n'a pas répondu.

On note également le code 100 qui est utiliser avec pour le label INFO.


**NB :** *La mention du mot "utilisateur" concerne de manière générale un client __standard__ tout comme un client qui aurait des droits d'__administrateur__. Il sera précisé si nécessaire, lorsque la situation sera spécifique à l'un ou l'autre.*

# 3. Connexion/Déconnexion <a name="co-deco"></a>

## 3.1. Connexion <a name="connexion"></a>

_Prérequis_ :

-   Compte/profil utilisateur créé par l'administrateur

L'utilisateur doit entrer le couple identifiant et mot de passe pour s'authentifier sur le serveur, et ainsi disposer de sa session.

<br>
<br>

### **Interface utilisateur et administrateur**

Pour se connecter sur le serveur, l'utilisateur ou l'administrateur doit au préalable s'authentifier (comme présenté ci-dessous).

``
bin/clientMain <nom.prenom@email.fr> <mot_de_passe>
``

### **Serveur**

Selon la situation, le serveur renvoie une confirmation de succès ou d'échec.

```
INFO : Client sur la machine d'adresse localhost connecte.
```



## 3.2. Déconnexion <a name="deco"></a>

Un utilisateur (administrateur ou non) doit pouvoir se déconnecter de l’application.
Pour se déconnecter, depuis le menu principal, il faudra entrer **8**.


### **Interface utilisateur**

Pour se déconnecter l’utilisateur doit entrer le **8** dans le menu d’accueil puis valider son choix.
La déconnexion implique la fermeture de la connexion entre le serveur et le client

Exemple :
```
Saisir le numéro de l'action voulu :
        > 8
deconnexion...

Vous avez été déconnecté.
```

### **Serveur**

A l'option **8** du menu d'accueil, le client envoie au serveur la requête `QUIT` et initialise la fermeture de la connexion.

Traitement du serveur :
```
DEBUG : Valeur de message : QUIT admin@email.fr
INFO : Traitement pour QUIT
```
<br>
<br>
<br>
<br>
<br>
<br>

# 4. Utilisateur <a name="utilisateur"></a>

## 4.1. Afficher son annuaire <a name="afficher_annuaire"></a>

L'annuaire adopte un principe de fonctionnement semblable à celui d'un répertoire de contacts. Aussi il est possible pour un utilisateur ou un administrateur de voir son propre annuaire.

### **Interface utilisateur**

Dans le menu d'accueil, l'utilisateur doit entrer **1** pour voir son propre annuaire.

Exemple :
```
Saisir le numéro de l'action voulu :
        > 1

Resultat :
Durand Antoine durand.antoine@email.fr 0606060606
Dupont Michel dupont.michel@email.fr	 0607070707
```

### **Serveur**

Le client envoie au serveur `GET <USER_COURANT>`.

Exemple :
```
DEBUG : Valeur de la ligne : Durand Antoine durand.antoine@email.fr 0606060606
DEBUG : Valeur de la ligne : Michel apeupres michelapeupres@email.fr 0607070707
DEBUG : Valeur de message : GET nom.prenom@email.fr
INFO : Traitement pour GET
```
Le serveur envoie au client son annuaire, s'il y a un code d'erreur, voir les [codes d'erreurs](#code_erreur) pour plus de détails.

## 4.2. Consulter un autre annuaire <a name="consulter_autre_annuaire"></a>

Un utilisateur non administrateur doit pouvoir consulter son annuaire et l'annuaire des personnes qui l'ont autorisé.

### **Interface utilisateur**

Dans le menu d'accueil, l'utilisateur choisit l'option **2** puis arrive sur une page où on lui demande l'identifiant de l'annuaire qu'il veut consulter. Pour que l'utilisateur puisse consulter cet annuaire il faut qu'il ait les droits en lecture sur cet annuaire.

Exemple :
```
Saisir le numéro de l'action voulu :
    > 2
De quel utilisateur voulez vous voir l'annuaire ?
    > dupont.toto@email.fr

Résultat :
Durand Antoine durand.antoine@email.fr 0606060606
Dupont Michel dupont.michel@email.fr 0607070707
```
### **Serveur**
Le client envoie une requete de la forme : GET `<USER_COURANT> <USER_ANNUAIRE_A_AFFICHER>`.

Exemple :
```
DEBUG : Valeur de message : GET admin@email.fr dupont.toto@email.fr
INFO : Traitement pour GET

DEBUG : Valeur de la ligne : Durand Antoine durand.antoine@email.fr 0606060606
DEBUG : Valeur de la ligne : Dupont Michel dupont.michel@email.fr 0607070707
```
Le serveur va alors renvoyer les lignes de l'annuaire que l'utilisateur voulait consulter, si ce dernier a les droits pour le consulter (voir exemple ci-dessus dans interface utilisateur). Sinon il renverra un code d'erreur voir les [codes d'erreurs](#code_erreur) pour plus de détails.

## 4.3. Rechercher dans son annuaire <a name="search"></a>
Tout utilisateur a  la possibilité d'effectuer une recherche dans son annuaire, pour retrouver les informations sur le contact souhaité.

### **Interface utilisateur**
Sur le menu principal, l'utilisateur entre l'option **3**.
```
Saisir le numéro de l'action voulu :
	> 3
Chercher : jamel

Resultat :
Element trouvé : Jamel Debouze jamel.debouze@email.fr 0302010405

```
### **Serveur**
Le serveur reçoit la requête `SEARCH <USER_COURANTt> <RECHERCHE>`.

Exemple :
```
DEBUG : Valeur de message : SEARCH durand.toto@email.fr jamel
INFO : Traitement pour SEARCH

DEBUG : Element trouvé : Debouze Jamel debouze.jamel@email.fr 0302010405
```
Une fois qu'il a traité la demande, le serveur envoie au client le résultat trouvé (voir l'exemple ci-dessus dans interface utilisateur), ou un code d'erreur, voir les [codes d'erreurs](#code_erreur) pour plus de détails.

<br>
<br>
<br>
<br>


## 4.4. Ajouter un contact <a name="add_contact"></a>

Un utilisateur doit pouvoir ajouter une ligne dans son annuaire.

### **Interface utilisateur**

Sur le menu d'accueil, l'utilisateur choisit l'option **4** et arrive sur une page contenant la liste numérotée de ses contacts. Il est alors invité à saisir respectivement les champs `<NOM> <PRENOM> <EMAIL> <TEL>` en ajoutant **un seul** espace entre chaque champ.

Exemple :
```
Saisir le numéro de l'action voulu :
        > 4
Saisir les informations à ajouter sous la forme : <NOM> <PRENOM> <EMAIL> <TEL>
        Ntamack Romain ntamack.romain@email.fr 0610152530

Resultat :
Contact ajouté avec succès
```
### **Serveur**

Le saisie du client envoie au serveur la requête sous la forme `ADD <USER_COURANT> <NOM> <PRENOM> <EMAIL> <TEL>`.

Exemple :
```
DEBUG : Valeur de message : ADD dupont.antoine@emal.fr Ntamack Romain ntamack.romain@email.fr 0610152530
INFO : Traitement pour ADD
```
Le serveur va ensuite renvoyer au client le résultat de l'opération (voir l'exemple ci dessus dans interface utilisateur), s'il y a un code d'erreur, voir [codes d'erreurs](#code_erreur) pour plus de détails.

## 4.5. Modification d'une ligne de l'annuaire <a name="modif_ligne"></a>

Un utilisateur doit pouvoir modifier une ligne de son annuaire.

### **Interface utilisateur**

Sur le menu d'accueil, l'utilisateur choisit l'option **5** et arrive sur une page contenant la liste de ses contacts précédés d'un numéro. On lui demande ensuite de saisir le numéro de ligne qu'il veut modifier, suivi du champ (NOM/PRENOM/NUM) (**Le champ NUMERO est immuable**) puis de la valeur de substitution.

Exemple :
```
Saisir le numéro de l'action voulu :
        > 5
Votre annuaire va s'afficher pour vous permettre de choisir une ligne à editer:

        1. Sanchez Antoine sanchez.antoine@email.fr 0606060606
        2. Durand Michel durand.michel@email.fr 0607070707
        3. toto tata toto.tata@email.fr 0505050505

Pour editer une ligne, saisir son numéro, le champ a modifier et sa valeur: <NUMERO DE LIGNE> <CHAMP> <VALEUR>
        > 2 nom Dupont

Resultat :
Contact modifié avec succès
```

### **Serveur**

Le client envoie d'abord au serveur une requête `GET <USER_COURANT>`
Le client envoie au serveur `EDIT <USER_COURANT> <NUMERO_LIGNE> <CHAMP> <VALEUR>` ce qui a pour effet de supprimer la ligne NUMERO de l'annuaire USER et de la recréer avec les nouvelles valeurs.

Exemple :
```
DEBUG : Valeur de message : GET nom.prenom@email.fr
INFO : Traitement pour GET

DEBUG : Valeur de la ligne : Sanchez Antoine sanchez.antoine@email.fr 0606060606
DEBUG : Valeur de la ligne : Durand Michel durand.michel@email.fr 0607070707
DEBUG : Valeur de la ligne : toto tata toto.tata@email.fr 0505050505
DEBUG : Valeur de message : EDIT nom.prenom@email.fr 2 nom dupont
INFO : Traitement pour EDIT

DEBUG : Valeur de la ligne 2 apres modification: Dupont Michel durand.michel@email.fr 0607070707
```
Le serveur va d'abord envoyer l'annuaire au client et une fois qu'il aura choisi la ligne à modifier en entrant le numéro de la ligne, le serveur pourra effectuer le traitement et renverra si l'opération est un succès, sinon il y aura un code d'erreur, voir les [codes d'erreurs](#code_erreur) pour plus de détails.


## 4.6. Suppression d'une ligne <a name="del_ligne"></a>

Chaque utilisateur peut s'il le souhaite, supprimer une ligne dans son annuaire.

### **Interface utilisateur**

Sur le menu d'accueil, l'utilisateur saisi **6**.

Exemple :
```
Saisir le numéro de l'action voulu :
        > 6
Votre annuaire va s'afficher pour vous permettre de choisir une ligne à supprimer :

        1. Sanchez Antoine sanchez.antoine@email.fr 0606060606
        2. Durand Michel durand.michel@email.fr 0607070707
        3. toto tata toto.tata@email.fr 0505050505

Pour supprimer une ligne, saisir son numéro :
        > 3

Resultat :
Contact supprimé avec succès
```
<br>
<br>
<br>
<br>

### **Serveur**

Le serveur reçoit du client la requête `DELETE <USER_COURANT> <CONTACT>`.

Exemple :
```
DEBUG : Valeur de message : GET sanchez.antoine@email.fr
INFO : Traitement pour GET
DEBUG : Valeur de la ligne : Sanchez Antoine sanchez.antoine@email.fr 0606060606
DEBUG : Valeur de la ligne : Durand Michel durand.michel@email.fr 0607070707
DEBUG : Valeur de la ligne : toto tata toto.tata@email.fr 0505050505
DEBUG : Valeur de message : DELETE sanchez.antoine@email.fr toto tata toto@email.fr 0505050505
INFO : Traitement pour DELETE toto.dupont@email.fr
```
Le serveur envoie ensuite au client le résultat de l'opération (voir exemple dans interface utilisateur), ou un code d'erreur, voir les [codes d'erreur](#code_erreur) pour plus de détails.

## 4.7. Afficher la liste des personnes qui ont des droits de lecture sur un annuaire <a name="afficher_liste_personne_droit"></a>

  Il est possible d'afficher, en tant qu'utilisateur(avec restrictions si non ADMIN), la liste des personnes qui auraient des droits de lecture sur un annuaire qu'il soit propre à l'utilisateur ou non (possible en tant qu'ADMIN).

### **Interface utilisateur**

  Dans le menu principal, l'utilisateur choisit l'option **7** pour pouvoir afficher la liste des personnes qui auraient des droits de lecture sur son annuaire.

  Exemple :
  ```
  Saisir le numéro de l'action voulu :
          > 7
  Affichage...

  Resultat :
  dupont.antoine@email.fr
  dupont.totot@email.fr
  admin@email.fr
  ```

### **Serveur**

  Le client envoie au serveur `GET_RIGHT <USER_COURANT> <USER_AUTORISE>`.

  Exemple :
  ```
  DEBUG : Valeur de message : GET_RIGHT sanchez.antoine@email.fr
  INFO : Traitement pour GET_RIGHT
  ```
Le serveur va en ensuite renvoyer au client les utilisateurs qui ont le droit de consulter son annuaire (voir exemple au dessus, dans interface utilisateur) si tout s'est bien déroulé ou un code d'erreur s'il y a un problème, voir les [codes d'erreur](#code_erreur) pour plus de détails.


<br>



## 4.8. Autoriser une personne à consulter mon annuaire <a name="autoriser_annuaire"></a>

  Un utilisateur aura la possibilité d'autoriser un autre utilisateur à accéder à son annuaire mais ce dernier aura seulement les droits en lecture.

### **Interface utilisateur**

  Dans le menu d'accueil, l'utilisateur choisit l'option **9**.
  L'utilisateur qui souhaite partager son annuaire devra alors rentrer l'adresse email de l'utilisateur à qui il veut partager son annuaire. L'adresse email sera du type `nom.prenom@email.fr`

  Exemple :
  ```
  Saisir le numéro de l'action voulu :
          > 9
  Saisir l'adresse mail de la personne à qui vous voulez ajouter les droits de lecture sur votre annuaire
          > dupont.toto@email.fr
  Resultat :
  Utilisateur ajouté avec succès
  ```

### **Serveur**

  Le serveur reçoit `GET_GRANT <USER_COURANT> <USER_A_AUTORISER>`.

  Exemple :

```
DEBUG : Valeur de message : GRANT nom.prenom@email.fr dupont.toto@email.fr
INFO : Traitement pour GRANT
```
Le serveur envoie au client si l'opération s'est déroulé avec succès (voir l'exemple ci dessus dans interface utilisateur). Dans le cas contraire, il y aura un code d'erreur, voir les [codes d'erreur](#code_erreur) pour plus de détails.

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>






# 5. Partie administrateur <a name="admin"></a>

## 5.10. Créer un utilisateur <a name="add_user"></a>

  Un administrateur a la possibilité d'ajouter de nouveaux utilisateurs.

### **Interface administrateur**

L'administrateur doit choisir l'option **10** quand il est sur le menu principal.
Il faudra ensuite saisir l'email suivi du mot de passe avec un **espace** entre l'email et le mot de passe. Voici le format : `<NEW_USER>` `<PASSWORD>`. **NEW_USER sera une adresse email qui prendra cette forme : nom.prenom@email.fr**

Exemple :
```
Saisir le numéro de l'action voulu :
        > 10
Saisir l'adresse mail et le mot de passe de la personne à ajouter
        > dupont.toto@email.fr motdepasse
Resultat :
Utilisateur crée avec succès.
```

### **Serveur**

Le serveur reçoit `ADD_USR <ADMIN_USER> <NEW_USER> <PASSWORD>`.

Exemple :

```
DEBUG : Valeur de message : ADD_USR admin@email.fr dupont.toto@email.fr motdepasse
INFO : Traitement pour ADD_USR
```

Le serveur enverra ensuite la confirmation à l'administrateur que l'utilisateur a bien été créé (voir exemple ci-dessus dans la partie interface administrateur) ou un code d'erreur en cas de problème, voir les [codes d'erreur](#code_erreur) pour plus de détails.


## 5.11. Supprimer un utilisateur <a name="del_user"></a>

L'administrateur a la possibilité de supprimer des utilisateurs.

### **Interface administrateur**
L'administrateur doit rentrer **11** quand il est sur le menu principal. Puis le menu présenté suivant en exemple sera affiché. Puis il devra rentrer l'adresse email à supprimer comme dans l'exemple ci-dessous.

Exemple :
```
Saisir le numéro de l'action voulu :
        > 11
Saisir l'adresse mail de l'utilisateur à supprimer
        > dupont.toto@email.fr
Resultat :
Utilisateur supprimé avec succès
```

### **Serveur**

Le serveur reçoit `DEL_USR <ADMIN_USER> <USER_SUPPRIMER>`.

Exemple :
```
DEBUG : Valeur de message : DEL_USR admin@email.fr dupont.toto@email.fr
INFO : Traitement pour DEL_USR
```
Le serveur renvoie à l'administrateur que l'utilisateur a bien été supprimé (voir exemple dans interface administrateur ci-dessus) ou un code d'erreur s'il y a un problème. Voir les [codes d'erreur](#code_erreur) pour plus de détails.



## 5.12. Afficher tous les utilisateurs <a name="all_user"></a>

Pour gérer les différents utilisateurs, il est préférable d'avoir une vue d'ensemble, l'administrateur a donc la possibilité d'afficher la liste de tous les utilisateurs (donc des adresses emails).

### **Interface administrateur**
L'administrateur doit rentrer **12** quand il est sur le menu principal.
La liste de tous les utilisateurs va alors s'afficher comme ceci.
```
Saisir le numéro de l'action voulu :
        > 12
Résultat:
nom.prenom1@email.fr
nom.prenom2@email.fr
nom.prenom3@email.fr
```
### **Serveur**

Le serveur reçoit une requete de la forme `GET_ALL <ADMIN_USER>`
```
DEBUG : valeur de message : GET_ALL admin@email.fr
INFO : Traitement pour GET_ALL
```
Le serveur enverra ensuite à l'administrateur la liste de tous les utilisateurs (voir interface administrateur ci-dessus). S'il renvoit un code d'erreur, voir les [codes d'erreurs](#code_erreur) pour plus de détails.
