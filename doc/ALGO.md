# Algorithmes du projet d'annuaires partagés
_Fait par Emmanuel HOUNZANGBE, Tristan VIVIER, Bastien SIBOUT, Tristan LOPEZ_
Version : 1.0.0

Dans les algorithmes, nous ne détaillerons pas les fonctions des librairies client.C et serveur.C données lors du TP 2.


<!-- TOC -->

- [Algorithme serveur](#algorithme-serveur)
- [Algorithme client](#algorithme-client)
- [Niveau 2:  Algorithme des fonctions](#niveau-2--algo-des-fonctions)
    - [serveur](#serveur)
        - [traiterPDU](#traiterpdu)
        - [typeRequete](#typerequete)
        - [verrifierArguments](#verrifierarguments)
        - [isPassValid](#ispassvalid)
    - [client](#client)
        - [afficherMenuAccueil](#affichermenuaccueil)
        - [getEtProcessChoixUser](#getetprocesschoixuser)
        - [ecrireSousMenu](#ecriresousmenu)

<!-- /TOC -->

# Algorithme serveur

```
// Rôle : décrit le fonctionnement du serveur
Algorithme serveur
VARIABLES

    Boolean fini
    Chaine message
    Chaine typeRequete

DEBUT
    //démarrage du service sur un port donné
    Initialisation()

    //boucle principale du serveur
    Tq (vrai){
        fini = FAUX
        Attendre_client()

        Tq (non fini){
            // on reçoit un message du client
            message = Recpetion()

            Si (message existe){
                // on récupère l'action de la PDU
                typeRequete = typeRequete(message)

                // on exécute le traitement relatif à l'action
                traiterPDU(typeRequete)
            } sinon
                fini = VRAI
        }

        //fermer la connexion client
        TerminaisonClient();
    }
FIN
```

<br>


# Algorithme client

```
// Rôle : décrit le fonctionnement du client
Algorithme client
VARIABLES

    Chaine pass
    Chaine identifiant

    Chaine message // totalité du message
    Chaine morceauCourant //morceau de message
    Entier nbArgs // nombre d'arguments de la fonction
DEBUT
    // on vérifie la présence et l'authenticité des arguments de connexion
    // identifiant, pass et nbArgs seront pris depuis la CLI
    verrifierArguments(identifiant, pass, nbArgs)

    // connexion avec le serveur
    Initialisation(serveur)

    Faire {
        // affichage du menu principale d'accueil
        afficherMenuAccueil(identifiant);

        //traitement du choix utilisateur
        getEtProcessChoixUser(userCourant)

        Faire {
            morceauCourant = Reception()
            //problème sur la Reception
            SI (morceauCourant est null){
                break;
            }

            // controle de la présence de fin de transmission
            SI (morceauCourant contient "\r\n"){
                fini = 1
            }
            // traitement du message
            // formation du message de reception
            message = message + morceauCourant


        }Tantque (!fini)

        ecrire("Resultat : " + message)
        /// remise à vide de message pour le prochain passage
        message = ""

    } tantque (vrai)
    retourne 0
FIN
```
<br>
<br>
<br>
<br>
<br>
<br>


# Niveau 2:  Algorithme des fonctions

## Serveur

### traiterPDU
```
// Rôle : Dispatcher les opérations relatives aux actionx de la PDU
FONCTION traiterPDU(Chaine typeRequete): Entier
VARIABLES


DEBUT
    SELON typeRequete FAIRE:
        # afficher annuaire
        "GET"     : retourne pduGet(requete)

        # ajouter une ligne
        "ADD"     : retourne pduAdd(requete)

        # editer une ligne
        "EDIT"    : retourne pduedit(requete)

        # supprimer une ligne
        "DELETE"  : retourne pduDelete(requete)

        # ajouter user droit d'affichage
        "GRANT"   : retourne pduGrant(requete)

        # ajouter user droit d'affichage
        "GET_RIGHT"   : retourne pduGetRight(requete)

        # recherche dans un annuaire
        "SEARCH"  : retourne pduSearch(requete)

        # deconnexion
        "QUIT"    : retourne pduQuit(requete)

        #### ADMIN ####
        # cree un user
        "CREATE"  : retourne pduCreate(requete)

        # del user
        "DEL_USR" : retourne pduDel_user(requete)

        # affiche tous les user
        "GET_ALL" :  retourne pduShowAll(requete)

        defaut: ecrire("403: L'impossible devient possible")

    retourne 1
FIN
```

### typeRequete
```
// Rôle : Dispatcher les opérations relatives aux actionx de la PDU
FONCTION typeRequete(Chaine requete): Chaine
VARIABLES

DEBUT
    //retourne le premier élément de la requête en séparant la chaine avec des " "
    retourne decoupeChaine(requete, " ")[1]
FIN
```

### verrifierArguments
```
// Rôle : contrôler l'authenticité du couple user pass
FONCTION verifierArguments(Chaine user, Chaine pass, nbArgs): Boolean
VARIABLES
DEBUT
    SI (nbArgs < 3){
        ecrire("Format attendu: bin/clientMain <email> <pass> référez vous à votre administrateur pour plus d'informations")
        retourne 1
    }
    SI (isPassValid(user, pass)){
        ecrire(Le couple <email> <pass> n'existe pas dans nos fichiers référez vous à votre administrateur pour plus d'informations")
        retourne 1
    }
    retourne 0
FIN
```

### isPassValid
```
// Rôle : contrôler l'authenticité du couple user pass
FONCTION isPassValid(Chaine user, Chaine pass, nbArgs): Boolean
VARIABLES

    Fichier auth
    Chaine passFichier

DEBUT
    // l'utilisateur n'existe pas
    SI (userExist(userExist) == 1){
        retourne 1
    }

    //ouverture et contrôle du fichier
    auth = ouvrirFichier("storage/" + user + "/auth" en lecture)
    SI (auth non exite){
        retourne 1
    }

    passFichier = lireLigneFichier()
    SI (passFichier = md5Hash(pass)){
        retourn 0;
    }

    retourne 1
FIN
```
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>


## Client

### afficherMenuAccueil

```
// Rôle: fonction d'affichage du menu
PROCEDURE afficherMenuAccueil(Chaine identifiant)
VARIABLES

DEBUT
    ecrire("Identifiant de l'utilisateur connecté : " identifiant)
    ecrire("\t1. Consulter son annuaire")
    ecrire("\t2. Consulter un annuaire")
    ecrire("\t3. Rechercher dans un annuaire")
    ecrire("\t4. Ajouter un contact")
    ecrire("\t5. Modifier un contact")
    ecrire("\t6. Supprimer un contact")
    ecrire("\t7. Afficher la liste des personnes autorisées à consulter mon annuaire")
    ecrire("\t8.Quitter")
    ecrire("\t9."Autoriser une personne à consulter son annuaire")

    // isAdmin demande au serveur si l'utilisateur courant est l'admin
    if (isAdmin(identifiant) == 0) { ;
      ecrire("\t#### ACTION ADMINISTRATEUR ####")
      ecrire("\t10. Créer un utilisateur")
      ecrire("\t11. Supprimer un utilisateur")
      ecrire("\t12. Afficher tous les utilisateurs")
    }

FIN
```

### getEtProcessChoixUser

```
//role : récupère et traite le choix du menuAccueil
PROCEDURE getEtProcessChoixUser(Chaine identifiant)
VARIABLES

    Entier choix // numéro du choix dans le menuAccueil
    tableau de Chaine inputScan // valeurs entée par l'utilisateur pour les sous menu

DEBUT

    ecrire("Saisir le numéro de l'action voulu :\n\t> ")

    //lire tant que le resultat n'est pas un entier
    tantque (lire(choix) est non Entier){
        ecrire("402 : Choix non valide, saisir le numéro de l'action")
    }

    //controle de la valeur choix: on a 12 choix possible
    SI ((isAdmin(email) == 1 && choix >= 10) || choix > 12 || choix <= 0){
        ecrire("402 : Choix non valide, saisir le numero de l'action")

        //retour case départ
        afficherMenuAccueil(identifiant);
        getEtProcessChoixUser(identifiant);
    } sinon {

        // traitement sous menu
        // 1 et 12 sont sans sous menu
        if (choix != 1 && choix != 12) {
          ecrireSousMenu(choix, email);
        }

        SELON choix FAIRE:
            /*Dans chaque cas il faut contrôler le nombre d'élements dans userInput et envoyer un code d'erreur si non correcte très répétitif donc on ne le met pas ici. De même pour la construction de la variable userInput qui est une concaténation de chaine de caractères récupérés dans lire() après contrôle.*/

            1 :
            // GET email (sans input car son annuaire)
            Emission("GET ", "GET " + identifiant)

            // GET email emailAutre
            2 :
            lire(inputScan)
            Emission("GET ", "GET " + identifiant + " " + inputScan)

            // SEARCH email PATERNE
            3 :
            lire(inputScan)
            Emission("SEARCH ", "SEARCH " + identifiant + " " + inputScan)

            // ADD email NOM PRENOM EMAIL TEL
            4 :
            lire(inputScan)
            Emission("ADD ", "ADD " + identifiant + " " + inputScan)

            // EDIT email LIGNE CHAMP VALEUR
            5 :
            lire(inputScan)
            Emission("EDIT ", "EDIT " + identifiant + " " + inputScan)

            // DELETE email LIGNE
            6 :
            lire(inputScan)
            Emission("DELETE ", "DELETE " + identifiant + " " + inputScan)

            // GET_RIGHT email emailAutre (emailAutre peux etre égale a email si non
            7 :
            Si (isAdmin(identifiant) == 0){
                lire(inputScan)
                Emission("GET_GRANT ", "GET_GRANT " + identifiant + " " + inputScan)
            } sinon {
                Emission("GET_GRANT ", "GET_GRANT " + identifiant)
            }


            8 :
            // QUIT(sans input)
            Emission("QUIT " + identifiant)

            // GRANT email emailAutre
            9 :
            lire(inputScan)
            Emission("GRANT ", "GRANT " + identifiant + " " + inputScan)


            ### ADMIN ###

            // ADD_USR email EmailToAdd Pass
            10 :
            lire(inputScan)
            Emission("CREATE ", "CREATE " + identifiant + " " + inputScan)

            // DEL_USR email EmailToDel
            11 :
            lire(inputScan)
            Emission("DEL_USR ", "DEL_USR " + identifiant + " " + inputScan)

            // GET_ALL email (sans input)
            12 :
            lire(inputScan)
            Emission("SHOW_ALL ", "SHOW_ALL " + identifiant + " " + inputScan)

            default : Emission("403 : L'impossible devient possible")


    }
)


FIN
```


### ecrireSousMenu

```
//Rôle : afficher le sous menu d'un choix de menuAccueil
PROCEDURE ecrireAction(Entier choix, Chaine identifiant)
VARIABLES

DEBUT

    SELON numeroChoix FAIRE:
        2 : ecrire("De quel utilisateur voulez vous voir l'annuaire ?")
        3 : ecrire("Chercher :")
        4 : ecrire("Saisir les informations à ajouter sous la forme : <NOM>* <PRENOM>* <EMAIL>* <COMMENTAIRE>")
        5 :
            ecrire("Votre annuaire va s'afficher pour vous permettre de choisir une ligne à editer: ")
            attendre(2)
            #Affiche son annuaire
            Emission("GET ", identifiant)
            ecrire("Pour éditer une ligne, saisir son numéro, le champ a modifier et sa valeur: <NUMERO DE LIGNE> <CHAMP> <VALEUR>")
        6 :
            ecrire("Votre annuaire va s'afficher pour vous permettre de choisir une ligne à supprimer: ")
            attendre(2)
            #Affiche son annuaire
            Emission("GET ", identifiant)
            ecrire("Pour supprimer une ligne, saisir son numéro")

        7 : ecrire("De quel annuaire voulez vous voir les droit ?")
        8 : ecrire("deconnexion")
        9 : ecrire("Saisir l'adresse mail de la personne a qui vous voulez ajouter les droits de lecture sur votre annuaire")
        10 : ecrire("Saisir l'adresse mail et le mot de passe de la personne à ajouter")
        11 : ecrire("Saisir l'adresse mail de l'utilisateur à supprimer")

FIN
```

Reste des fonctions pour le rafinnage de niveau 3:
* pduGet(Chaine requete)
* pduAdd(Chaine requete)
* pduedit(Chaine requete)
* pduDelete(Chaine requete)
* pduGrant(Chaine requete)
* pduGetRight(Chaine requete)
* pduSearch(Chaine requete)
* pduQuit(Chaine requete)
* pduCreate(Chaine requete)
* pduDel_user(Chaine requete)
* pduShowAll(Chaine requete)
