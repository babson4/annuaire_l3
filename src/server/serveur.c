#include "serveur.h"
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <netdb.h>
#include <openssl/md5.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>

#define TRUE 1
#define FALSE 0
#define LONGUEUR_TAMPON 8192
#define MAX 1024

/* le socket d'ecoute */
int socketEcoute;
/* longueur de l'adresse */
socklen_t longeurAdr;
/* le socket de service */
int socketService;
/* le tampon de reception */
char tamponClient[LONGUEUR_TAMPON];
int debutTampon;
int finTampon;

/* Initialisation.
 * Creation du serveur.
 */
int Initialisation() { return InitialisationAvecService("13214"); }

/* Initialisation.
 * Creation du serveur en precisant le service ou numero de port.
 * renvoie 1 si ea c'est bien passe 0 sinon
 */
int InitialisationAvecService(char *service) {
    int num;
    const int state = 1;
    struct addrinfo hints, *res, *ressave;
    bzero(&hints, sizeof(struct addrinfo));

    hints.ai_flags = AI_PASSIVE;
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    if ((num = getaddrinfo(NULL, service, &hints, &res)) != 0) {
        printf("Initialisation, erreur de getaddrinfo : %s", gai_strerror(num));
        return 0;
    }
    ressave = res;

    do {
        socketEcoute = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
        if (socketEcoute < 0)
            continue; /* error, try next one */

        setsockopt(socketEcoute, SOL_SOCKET, SO_REUSEADDR, (char *) &state,
                   sizeof(state));
#ifdef BSD
        setsockopt(socketEcoute, SOL_SOCKET, SO_REUSEPORT, &state, sizeof(state));
#endif
        if (bind(socketEcoute, res->ai_addr, res->ai_addrlen) == 0)
            break; /* success */

        close(socketEcoute); /* bind error, close and try next one */
    } while ((res = res->ai_next) != NULL);

    if (res == NULL) {
        perror("Initialisation, erreur de bind.");
        return 0;
    }

    /* conserve la longueur de l'addresse */
    longeurAdr = res->ai_addrlen;

    freeaddrinfo(ressave);
    /* attends au max 4 clients */
    listen(socketEcoute, 4);
    fprintf(stdout,
            CYAN "INFO :" NO_COLOR " Creation du serveur reussie sur %s.\n",
            service);

    return 1;
}

/* Attends qu'un client se connecte.
 */
int AttenteClient() {
    struct sockaddr *clientAddr;
    char machine[NI_MAXHOST];

    clientAddr = (struct sockaddr *) malloc(longeurAdr);
    socketService = accept(socketEcoute, clientAddr, &longeurAdr);
    if (socketService == -1) {
        perror("AttenteClient, erreur de accept.");
        return 0;
    }
    if (getnameinfo(clientAddr, longeurAdr, machine, NI_MAXHOST, NULL, 0, 0) ==
        0) {
        // debug
        fprintf(stdout,
                CYAN "INFO :" NO_COLOR
                " Client sur la machine d'adresse %s connecte.\n",
                machine);
    } else {
        // debug
        fprintf(stdout, CYAN "INFO :" NO_COLOR " Client anonyme connecte.\n");
    }
    free(clientAddr);
    /*
     * Reinit buffer
     */
    debutTampon = 0;
    finTampon = 0;

    return 1;
}

/* Recoit un message envoye par le serveur.
 */
char *Reception() {
    char message[LONGUEUR_TAMPON];
    int index = 0;
    int retour = 0;
    while (1) {
        /* on cherche dans le tampon courant */
        while ((finTampon > debutTampon) && (tamponClient[debutTampon] != '\n')) {
            message[index++] = tamponClient[debutTampon++];
        }
        /* on a trouve ? */
        if ((index > 0) && (tamponClient[debutTampon] == '\n')) {
            message[index++] = '\n';
            message[index] = '\0';
            debutTampon++;
            return strdup(message);
        }

        /* il faut en lire plus */
        debutTampon = 0;
        retour = recv(socketService, tamponClient, LONGUEUR_TAMPON, 0);
        if (retour < 0) {
            // perror("Reception, erreur de recv.");
            return NULL;
        } else if (retour == 0) {
            fprintf(stderr,
                    RED "Reception, le client a ferme la connexion." NO_COLOR "\n");
            return NULL;
        }
        /*
         * on a recu "retour" octets
         */
        finTampon = retour;
    }
    return NULL;
}

/* Envoie un message au client.
 * Attention, le message doit etre termine par \n
 */
int Emission(char *message) {
    int taille;
    if (strstr(message, "\n") == NULL) {
        fprintf(stderr, RED
                        "Emission, Le message n'est pas termine par \\n." NO_COLOR "\n");
        return 0;
    }

    taille = strlen(message);
    if (send(socketService, message, taille, 0) == -1) {
        perror("Emission, probleme lors du send.");
        return 0;
    }

    // debug
    // printf("Emission de %d caracteres.\n", taille + 1);
    return 1;
}

/* Ferme la connexion avec le client. */
void TerminaisonClient() { close(socketService); }

/* Arrete le serveur. */
void Terminaison() { close(socketEcoute); }

/** @author Bastien
 * Retourne le type d'action de la PDU requete[1] (GET,ADD,DELETE ...)
 */
char *typeRequete(char *requete) {
    // on recupère le type de requete: (AUTH, PUT, GET, GRANT.....)
    int curseur = (int) strcspn(requete, " ");

    // on stock le resutlat +s1 (\0)
    char *typeReq = calloc(curseur + 1, sizeof(char));
    // recopie la chaine requete dans typeReq avec curseur char max
    strncat(typeReq, requete, curseur);
    return typeReq;
}

/** @author Bastien
 * Fonction qui verifie si les inforamtion email pass existe et sont valide
 * AUTH email pass
 * 0 si non valide
 * 1 si valide
 */
int pduAuth(char *requete) {
    char user[80], pass[80];
    get_champ_n(requete, 2, user);
    get_champ_n(requete, 3, pass);

    // take off \n --> md5 hash
    strtok(pass, "\n");
    return isPassValid(user, pass);
}

/** @author Bastien
 * @def Fonction qui retourne si le couple email / pass existe
 * 0 en cas d'erreur
 * 1 si tout ok
 */
int isPassValid(char *email, char *pass) {
    char md5_hash[2 * MD5_DIGEST_LENGTH + 1] = "";
    FILE *authFile;
    char passInFile[MAX];

    if (userExist(email) != 1) {
        fprintf(stdout, YELLOW "user exist OK" NO_COLOR "\n");
        return 0;
    }


    // check the auth file of the user
    authFile = fopen(concat("storage/", concat(email, "/auth")), "r");
    if (authFile == NULL) {
        return 0;
    }

    // lecture de la première ligne du fichier
    fgets(passInFile, MAX, authFile);

    // remove \n from buffer
    strtok(passInFile, "\n");

    // tout est ok
    if (strcmp(passInFile, md5Hash(pass, md5_hash)) == 0) {
        return 1;
    }

    return 0;
}


/** @author Bastien
 * Fonction qui retourne si un utilisateur existe
 */
int userExist(char *email) {
    const char *folder;
    struct stat fileStruct;

    // secrure email
    removeSlash(email);

    folder = concat("storage/", concat(email, "/"));

    return stat(folder, &fileStruct) == 0 && S_ISDIR(fileStruct.st_mode);
}

/** @author Bastien
 * fonction qui concatère deux chaine puis retourne le résulatat
 * strcat ne fonctionne pas comme je le veux donc j'ai refait cette fonction
 */
char *concat(const char *str1, const char *str2) {

    char *result = malloc(strlen(str1) + strlen(str2) + 1); // +1 for the \0
    strcpy(result, str1);
    strcat(result, str2);
    return result;
}

/** @author Tristan LOPEZ // fix Bastien
 *FONCTION QUI AFFICHE UN ANNUAIRE
 * GET user_asking (user_cible)
 */
int pduGet(char *requete) {
    // Déclaration des variables
    char nom[80], prenom[80], mail[80], num_tel[20], user_asking[80],
            user_cible[80], type[10];
    char *annuaireFileName = NULL;
    char *returnValue = calloc(LONGUEUR_TAMPON + 1, sizeof(char));
    // Initialise le pointeur a NULL
    FILE *annuaire = NULL;
    int nbLigneAnnuaire = 0;

    // fix bastien
    // on a user_cible ?
    if (sscanf(requete, "%s %s %s", type, user_asking, user_cible) == 3) {
        // requete de la forme : GET user_asking user_cible
        // verif des droits
        if (userHaveRight(requete) == 1) {
            get_champ_n(requete, 3, user_cible);
            // remove \n
            strtok(user_cible, "\n");
            annuaireFileName = concat("storage/", concat(user_cible, "/annuaire"));
        } else {
            Emission("Vous n'avez pas le droit de consulter cet annuaire\n");
            Emission("\r\n");
            free(returnValue);
            return 1;
        }

    } else {
        // requete de la forme : GET user_asking
        get_champ_n(requete, 2, user_asking);
        // remove \n
        strtok(user_asking, "\n");

        annuaireFileName = concat("storage/", concat(user_asking, "/annuaire"));
    }

    annuaire = fopen(annuaireFileName, "r");
    // Lecture du fichier
    if (annuaire == NULL) {
        free(returnValue);
        Emission("Une erreur est survenue, réessayer plus tard\n");
        Emission("\r\n");
        return 1;
    }

    nbLigneAnnuaire = getNbLine(annuaire);
    for (int i = 0; i < nbLigneAnnuaire; i++) {
        // On lit le fichier ligne par ligne et on vérifie que le format a été
        // respecté dans le fichier texte donné
        if (fscanf(annuaire, "%s %s %s %s", prenom, nom, mail, num_tel) != 4) {
            continue;
        }
        // On concaténe les infos en prenant soin de mettre les espaces
        sprintf(returnValue, "%s %s %s %s\n", prenom, nom, mail, num_tel);
        if (Emission(returnValue) == -1) {
            // Si un échec est détecté, on abandonne et on retourne 0 au programme
            // appelant
            fprintf(stderr,
                    RED "Erreur, lors de l'envoi de returnValue" NO_COLOR "\n");
            fclose(annuaire);
            return 1;
        }
        fprintf(stdout,
                MAGENTA "DEBUG :" NO_COLOR " Valeur de la ligne : " WHITE
                "%s" NO_COLOR,
                returnValue);
    }
    // On ferme le fichier
    fclose(annuaire);

    // fin de requete
    free(returnValue);
    Emission("\r\n");
    return 0;
}

/** @author Tristan LOPEZ // fix: Bastien
 * FONCTION QUI AJOUTE UNE LIGNE A UN ANNUAIRE
 * ADD email NOM PRENOM EMAIL TEL
 */
int pduAdd(char *requete) {
    // Déclaration des variables
    char nomAdd[20], prenom[20], mail[40], numeroTel[11], emailCourant[40];

    // On initialise le compteur à NULL pour être sûr que fopen s'est bien déroulé
    FILE *annuaire = NULL;

    get_champ_n(requete, 2, emailCourant);

    // secrure email
    removeSlash(emailCourant);

    get_champ_n(requete, 3, nomAdd);
    get_champ_n(requete, 4, prenom);
    get_champ_n(requete, 5, mail);
    get_champ_n(requete, 6, numeroTel);
    strtok(numeroTel, "\n");

    annuaire = fopen(concat("storage/", concat(emailCourant, "/annuaire")), "a");

    // On commence si le fichier s'est bien ouvert
    if (annuaire == NULL) {
        // envoie erreur
        Emission(RED "Erreur:" NO_COLOR " lors de l'ouverture du fichier.\n");
        Emission("\r\n");
    } else {
        // ecriture
        fprintf(annuaire, "%s %s %s %s\n", nomAdd, prenom, mail, numeroTel);
        fclose(annuaire);

        // Envoie de la réponse au client
        Emission(GREEN "200 : Données ajoutées avec succès" NO_COLOR "\n");
        Emission("\r\n");
    }
    return 0;
}

/** @author Tristan LOPEZ && Bastien
 *FONCTION QUI SUPPRIME UN UTILISATEUR
 * DEL_USR email EmailToDel
 */
int pduDelUser(char *requete) {
    char emailToDel[80];
    int returnValue = 0;
    // recuperation des info de la requete
    get_champ_n(requete, 3, emailToDel);

    // secrure email
    removeSlash(emailToDel);
    strtok(emailToDel, "\n"); // retire \n

    // supprime les fichiers
    returnValue = delFiles(concat("storage/", emailToDel));

    if (returnValue == 0) {
        Emission(GREEN "200 : Utilisateur supprimé avec succès. " NO_COLOR "\n");
        Emission("\r\n");
    } else {
        Emission(RED "Une erreur est survenue, réessayer plus tard." NO_COLOR "\n");
        Emission("\r\n");
    }
    return returnValue;
}

/** @author Bastien
 * supprime les fichier du dossier "pathDossier"
 */
int delFiles(char *pathDossier) {
    char fileName[][10] = {"/annuaire", "/auth", "/perm"};
    // iteration sur fileName (division avec sizeof pour connaitre le nombre
    // d'elements dans le tableau)
    for (size_t i = 0; i < sizeof(fileName) / sizeof(fileName[0]); i++) {
        if (remove(concat(pathDossier, fileName[i])) != 0) {
            return errno;
        }
    }
    return remove(pathDossier);
}

/// @author Tristan Vivier
/// fonction qui copie dans la variable d'entrée champ le champ voulu dans la
/// requête utilisation : n correspond à la place dans une requête : "text1 text2 text3"
/// text1 sera à la place numéro 1 par ex
void get_champ_n(char *requete, int num, char *champ) {
    char *requeteBis = malloc((strlen(requete) + 1) * sizeof(char)); //+1 pour \0
    int numCourant = 1;
    char delim[2] = " ";
    char *ptr;

    /// copie de la requete dans une nouvelle variable car strtok modifie la
    /// chaîne
    strcpy(requeteBis, requete);

    /// on mais ptr au début de la chaîne requeteBis
    ptr = strtok(requeteBis, delim);
    /// tant qu'on a pas atteint la position voulue on continue
    while (numCourant < num) {
        ptr = strtok(NULL, delim);
        numCourant++;
    }
    /// on copie dans la variable de sortie
    strcpy(champ, ptr);
    free(requeteBis);
}

/// @author Tristan Vivier
/// sous-fonction utilisée par pduEdit() qui retourne une ligne voulue dans un
/// fichier
// fix bastien : controle si numeroLigne n'est pas suppérieur aux nombre de
// ligne max du fichier (sinon seg fault)
int recupereLigne(char *fichier, char *ligne, int numeroLigne) {
    /// déclaration des variables
    FILE *file = NULL;
    int numLigneEnCours = 1;
    char ligneEnCours[120];

    /// on ouvre le fichier
    file = fopen(fichier, "r");

    /// si l'ouverture a réussi on continue
    if (file == NULL) {
        return 0;
    }

    /// tant qu'on est pas à la bonne ligne on continue
    while (numLigneEnCours < numeroLigne) {
        /// on récupère une ligne du fichier
        fgets(ligneEnCours, 120, file);
        numLigneEnCours++;
    }
    /// on retourne la bonne ligne en sortie
    fgets(ligne, 120, file);

    /// on ferme le fichier
    fclose(file);
    return 1;
}

/// @author Tristan Vivier // fix bastien
/// sous-fonction utilisée par pduEdit() qui remplace une partie d'une ligne,
/// les deux sont données en arguments
int editeLigne(char *ancienneLigne, char *nouvelleLigne, int modif,
               char *nouveauChamp) {
    /// déclaration des variables
    int compteur = 1;
    char *ptr;
    char delim[2] = " ";

    /// on se place en début de ligne
    ptr = strtok(ancienneLigne, delim);

    /// tant qu'on est pas sur le bon champ on continue et on copie dans
    /// nouvelleLigne
    while (compteur < modif) {
        strcat(nouvelleLigne, ptr);
        strcat(nouvelleLigne, " ");
        ptr = strtok(NULL, delim);
        compteur++;
    }

    /// on copie le nouveau champ dans nouvelleLigne
    strcat(nouvelleLigne, nouveauChamp);
    strcat(nouvelleLigne, " ");
    ptr = strtok(NULL, delim);
    compteur++;

    /// on finit de copier la ligne
    while (ptr != NULL) {
        strcat(nouvelleLigne, ptr);
        strcat(nouvelleLigne, " ");
        ptr = strtok(NULL, delim);
    }

    // fix bastien
    // le bloc while du dessus ajoute un " " en trop à la fin de la chaine je
    // coupe donc la chaine un char plus tôt; moche mais fonctionnel
    nouvelleLigne[strlen(nouvelleLigne) - 1] = '\0';
    return 1;
}

/// @author Tristan Vivier
/// edite un annuaire suivant une requête de la forme EDIT user numero_ligne
/// champ nouveau_champ
int pduEdit(char *requete) {
    // déclaration des variables
    int len = strlen(requete); // --> Bastien : -Wstack-protector trigger
    // (l'idéal serais de passer avec des char* et des malloc alloué dans le programme appelant)
    FILE *annuaire = NULL;
    FILE *annuaireTemp = NULL;
    char champ[len];
    char user[len];
    char changement[len];
    char ligne[len];
    int modif = 0;
    char nomFichier[1024] = {0};
    char temp[14] = "storage/temp";
    char str[1024];
    int ligneCompteur = 0;
    char NouvelleLigne[1024] = {0};
    char AncienneLigne[1024];
    int intLigne = 0;

    /// on récupère les champs voulus
    get_champ_n(requete, 2, user);
    removeSlash(user);
    strtok(user, "\n");
    get_champ_n(requete, 3, ligne);
    strtok(ligne, "\n");
    get_champ_n(requete, 4, champ);
    strtok(champ, "\n");
    get_champ_n(requete, 5, changement);
    strtok(changement, "\n");
    intLigne = atoi(ligne);

    /// on récupère le champ qu'on veut changer, la variable modif servira dans
    /// une sous-fonction ultérieure
    // fix bastien : ajout du changement de case et EMAIL
    if (strcmp(strToUpper(champ), "PRENOM") == 0) {
        modif = 1;
    } else if (strcmp(strToUpper(champ), "NOM") == 0) {
        modif = 2;
    } else if ((strcmp(strToUpper(champ), "MAIL") == 0) ||
               strcmp(strToUpper(champ), "EMAIL") == 0) {
        modif = 3;
    } else if (strcmp(strToUpper(champ), "NUM") == 0) {
        modif = 4;
    } else {
        modif = 5;
    }
    /// on récupère le nom du fichier à éditer
    snprintf(nomFichier, strlen(requete) + 1024, "storage/%s/annuaire", user);

    /// on ouvre le fichier annuaire
    annuaire = fopen(nomFichier, "r");

    // fix bastien
    // controle du nombre de ligne
    if (getNbLine(annuaire) < intLigne) {
        fprintf(stdout, RED "ERREUR :" NO_COLOR " la ligne saisie est trop grande");
        Emission(RED "ERREUR :" NO_COLOR "Numéro de ligne saisie inalide\n");
        Emission("\r\n");
        return 0;
    }

    // creation / ouverture du fichier temp
    annuaireTemp = fopen(temp, "w");

    /// on récupère la ligne à modifier puis on la modifie
    recupereLigne(nomFichier, AncienneLigne, intLigne);

    editeLigne(AncienneLigne, NouvelleLigne, modif, changement);
    fprintf(stdout,
            MAGENTA "DEBUG :" NO_COLOR
            "valeur de la ligne %d apres modification: %s" NO_COLOR "\n",
            intLigne, NouvelleLigne);

    /// on réécrit le fichier annuaire dans le fichier annuaireTemp SAUF la ligne
    /// à modifier
    if (annuaire != NULL && annuaireTemp != NULL) {

        while (!feof(annuaire)) {
            strcpy(str, "\0");
            fgets(str, 80, annuaire);
            if (!feof(annuaire)) {
                ligneCompteur++;
                if (ligneCompteur == intLigne) {
                    fprintf(annuaireTemp, "%s", NouvelleLigne);
                } else {
                    fprintf(annuaireTemp, "%s", str);
                }
            }
        }
    } else {
        Emission(RED "500 :" NO_COLOR
                 " Une erreur est survenue, réessayer plus tard\n");
        Emission("\r\n");
        return 0;
    }

    /// on ferme les deux fichiers puis on supprime annuaire et on renomme
    /// annuaireTemp en annuaire
    fclose(annuaire);
    fclose(annuaireTemp);
    remove(nomFichier);
    rename(temp, nomFichier);
    Emission(GREEN "200 : Contact modifié avec succès" NO_COLOR "\n");
    Emission("\r\n");
    return 1;
}

/// @author Tristan Vivier
// ajout controle admin: Bastien
/// lit le fichier ./storage/$USER$/perm avec le $USER$ donné dans le 3ème champ
/// de la requête requête de la forme GET_GRANT user_connecté user_à_lire
int pduGetRight(char *requete) {
    /// déclaration des variables
    int len = strlen(requete) * 3;
    char nomFichier[80] = {0};
    char cible[len];
    char email[len];
    char str[80];
    FILE *file = NULL;

    // recuperation de l'email
    get_champ_n(requete, 2, email);
    removeSlash(email);
    strtok(email, "\n");

    if (isAdmin(email) == 0) {
        /// on récupère le nom de l'annuaire auquel on veut consulter les droits
        get_champ_n(requete, 3, cible);
        removeSlash(cible);
        strtok(cible, "\n");
    } else {
        strcpy(cible, email);
        removeSlash(cible);
    }

    snprintf(nomFichier, len, "storage/%s/perm", cible);

    /// on ouvre le fichier voulu contenant les droits pour l'annuaire voulu
    file = fopen(nomFichier, "r");

    /// on envoie le contenu du fichier au client
    if (file == NULL) {
        return 1;
    }

    while (!feof(file)) {
        strcpy(str, "\0");
        fgets(str, 80, file);
        Emission(concat(str, "\n"));
    }
    Emission("\r\n");
    fclose(file);
    return 0;
}

/// @author Tristan Vivier
/// ajoute un user dans le perm d'un annuaire lié, requête de la forme GRANT
/// GRANT user_connecte user_à_ajouter
int pduGrant(char *requete) {
    /// déclaration des variables
    int len = strlen(requete) * 2;
    char cible[len];
    FILE *file = NULL;
    char nomFichier[80] = {0};
    char nouvellePerm[len];

    /// on récupère les champ voulus : l'annuaire dont on modifie les droits puis
    /// la nouvelle personne à ajouter
    get_champ_n(requete, 2, cible);
    removeSlash(cible);
    get_champ_n(requete, 3, nouvellePerm);
    // remove \n and '/' from nouvellePerm
    removeSlash(nouvellePerm);
    strtok(nouvellePerm, "\n");
    snprintf(nomFichier, len, "storage/%s/perm", cible);
    /// on ouvre le fichier des droits de l'annuaire voulu
    file = fopen(nomFichier, "a+");

    if (file == NULL) {
        Emission(RED "Erreur:" NO_COLOR "Impossible d'ajouter cet utilisateur.");
        Emission("\r\n");
        return 1;
    }

    // ecriture sur le fichier
    fprintf(file, "%s\n", nouvellePerm);
    // retour au client
    Emission(GREEN "utilisateur ajouté avec succès" NO_COLOR "\n");
    Emission("\r\n");
    fclose(file);
    return 0;
}

/** @author Emmanuel
 * fix && md5 : bastien
 * Fonction qui permet de crée un utilisateur pour l'annuaire
 * cree le repertoire de l'utilisateur et tous les fichiers nécessaire
 * pour l'appli.
 * ADD_USR email EmailToAdd PassToAdd
 */
int pduUserCreate(char *requete) {
    char emailToAdd[MAX];
    char passToAdd[MAX];

    FILE *fichier;
    char fileName[][10] = {"annuaire", "auth", "perm"};
    char *filePath;
    // hash MD5 de pass
    char md5_hash[2 * MD5_DIGEST_LENGTH + 1] = "";

    // recuperation des infos dans la requete
    get_champ_n(requete, 3, emailToAdd);

    // on retire tous les "/" pour éviter les problemes
    removeSlash(emailToAdd);

    get_champ_n(requete, 4, passToAdd);
    strtok(passToAdd, "\n");

    if (userExist(emailToAdd)) {
        Emission("L'utlisateur est déjà existant\n");
        Emission("\r\n");
        return 1;
    }

    // creation du repertoire avec des droits uniquement sur le user
    mkdir(concat("storage/", emailToAdd), 0700);
    filePath = concat("storage/", concat(emailToAdd, concat("/", "")));

    // creation annuaire
    fichier = fopen(concat(filePath, fileName[0]), "w+");
    if (fichier == NULL) {
        Emission("Erreur d'ouverture annuaire\n");
        Emission("\r\n");
        return (errno);
    }
    fclose(fichier);

    // creation auth
    fichier = fopen(concat(filePath, fileName[1]), "w+");
    if (fichier == NULL) {
        Emission("Erreur d'ouverture auth\n");
        Emission("\r\n");
        return (errno);
    }
    fprintf(fichier, "%s", md5Hash(passToAdd, md5_hash));
    fclose(fichier);

    // creation perm
    fichier = fopen(concat(filePath, fileName[2]), "w+");
    if (fichier == NULL) {
        printf("Erreur d'ouverture perm\n");
        Emission("\r\n");
        return (errno);
    }
    fprintf(fichier, "%s", emailToAdd);
    fclose(fichier);

    Emission(GREEN "200 : utilisateur crée avec succès" NO_COLOR "\n");
    Emission("\r\n");
    return 0;
}

/** @author Bastien
 * supprime les "/"  de string
 */
void removeSlash(char *string) {
    // variables poubelle
    char *tempA;
    char *tempB;

    for (tempA = tempB = string; (*tempB = *tempA); tempB += (*tempA++ != '/'));
}

/** @author Bastien
 * Fonction qui retourne une chaine contenant le hash MD5 d'une chaine
 */
char *md5Hash(char *string, char *hash) {
    char unsigned md5[MD5_DIGEST_LENGTH] = {0};

    MD5((const unsigned char *) string, strlen(string), md5);

    for (int i = 0; i < MD5_DIGEST_LENGTH; i++) {
        sprintf(hash + 2 * i, "%02x", md5[i]);
    }
    return hash;
}

/** @author Bastien
 * Fonction qui cherche un paterne dans un fichier d'annuaire et affiche
 * la ligne à l'utilisateur
 */
void pduRecherche(char *requete) {

    char email[40];
    char paterne[50];

    // recuperation des donneee depuis la requete
    get_champ_n(requete, 2, email);
    get_champ_n(requete, 3, paterne);
    // formatage
    strtok(email, "\n");
    strtok(paterne, "\n");

    removeSlash(email);

    char *annuaireFileName = concat("storage/", concat(email, "/annuaire"));

    FILE *fileAnnuaire;
    fileAnnuaire = fopen(annuaireFileName, "rw");
    if (fileAnnuaire == NULL) {
        fprintf(stdout,
                RED "erreur sur l'ouverture du fichier %s, erreur numero : "
                "%d\n" NO_COLOR,
                annuaireFileName, errno);
        Emission("Une erreur est survenue, réessayer plus tard\n");
        Emission("\r\n");
    } else {
        char bufferRecherche[MAX];
        char *paterneFound = NULL;
        int resultat = FALSE;

        // on cherche le patern
        while (fgets(bufferRecherche, MAX, fileAnnuaire) != NULL) {
            // recherche un parten dans une ligne
            paterneFound = strcasestr(bufferRecherche, paterne);
            if (paterneFound) {
                // paterne trouvé dans cette ligne: on le print
                fprintf(stdout,
                        MAGENTA "DEBUG :" NO_COLOR " Element trouvé " WHITE
                        ": %s" NO_COLOR "\n",
                        bufferRecherche);
                Emission(concat("Element trouvé : " WHITE,
                                concat(bufferRecherche, NO_COLOR "\n")));
                resultat = TRUE;
            }
        }
        Emission("\r\n");
        if (!resultat) {
            fprintf(stdout,
                    CYAN "Aucun résultat trouvé pour le mot clé donné\n" NO_COLOR);
            Emission("Aucun résultat trouvé pour le mot clé donné\n");
            Emission("\r\n");
        }
    }
    fclose(fileAnnuaire);
}

/// fait par Vivier
/// vérifie que le user qui demande a le droit de consulter l'annuaire demandé
/// retourne 1 si l'utilisateur demandant a le droit; 0 sinon
/// pour rappel la requete sera de la forme GET user_asking user_cible
int userHaveRight(char *requete) {
    /// déclaration des variables
    int len = strlen(requete);
    char user_cible[len];
    char user_asking[len];
    FILE *file = NULL;
    char nomFichier[80] = {0};
    char str[80];

    get_champ_n(requete, 2, user_asking);
    get_champ_n(requete, 3, user_cible);

    // retire \n de user_cible && user_asking
    strtok(user_cible, "\n");
    strtok(user_asking, "\n");
    removeSlash(user_cible);
    removeSlash(user_asking);

    snprintf(nomFichier, len, "storage/%s/perm", user_cible);

    // fprintf(stdout, MAGENTA "DEBUG userHaveRight : " NO_COLOR "nomFichier :
    // %s\n", nomFichier);
    // fprintf(stdout, MAGENTA "DEBUG userHaveRigh : " NO_COLOR "user_asking :
    // %s\n", user_asking);

    file = fopen(nomFichier, "r");

    if (file == NULL) {
        /// si on ne peut pas ouvrir le fichier on retourne -1
        return -1;
    }

    while (!feof(file)) {
        fgets(str, 80, file);
        // printf("str : %s\n", str);
        /// on regarde si l'utilisateur demandant est dans le fichier
        if (strstr(str, user_asking) != NULL) {
            return 1;
        }
    }

    /// si on arrive ici c'est que le fichier a ét�� ouvert puis lu entièrement
    /// sans trouver le user_asking il n'a donc pas le droit
    return 0;
}

/** @author Bastien
 * Fonction retourne la chaine <string> en majuscule
 */
char *strToUpper(const char *string) {
    char *result = malloc(strlen(string) + 1 * sizeof(char));
    strcpy(result, string);
    for (int i = 0; i < (int) strlen(result); i++) {
        result[i] = toupper(result[i]);
    }
    return result;
}

/** @author Bastien
 * Retourne le nombre de ligne d'un fichier
 */
int getNbLine(FILE *file) {
    int nbLine = 0;
    while (!feof(file)) {
        if (fgetc(file) == '\n') {
            nbLine++;
        }
    }

    rewind(file);
    return nbLine;
}

/** @author Bastien
 * retourne 0 si l'utilisateur est admin
 */
int isAdmin(char *email) { return strcmp("admin@email.fr", email) != 0; }

/** @author Tristan Lopez
 * Permet d'afficher la liste de tous les utilisateurs
 * GET_ALL email
 */
void pduShowAll() {
    struct dirent *dir;
    // On ouvre le dossier //
    DIR *dirPtr = opendir("storage/");
    if (dirPtr) {
        while ((dir = readdir(dirPtr)) != NULL) {
            // Pour ne pas afficher le dossier courant et le dossier parent //
            if (strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0) {
                continue;
            }
            Emission(concat(dir->d_name, "\n"));
            // On ajoute le répertoire emailCourant dans returnValue
        }
        // On ferme le dossier //
        closedir(dirPtr);
    }
    Emission("\r\n");
}

// @author Emmanuel | fix Bastien
// fonction qui permet de supprimer une ligne d'un annuaire
// DELETE email ligne
int pduDelete(char *requete) {
    char email[80];
    char ligne[3];
    char temp[14] = "storage/temp";
    int countLigne = 0;
    int initLigne;
    char *curr = calloc(1024, sizeof(char));
    FILE *fileAnnuaire;
    FILE *fileAnnuaireTemp;

    //recuperation variables de la requete
    get_champ_n(requete, 2, email);
    // mise en forme
    strtok(email, "\n");
    removeSlash(email);

    get_champ_n(requete, 3, ligne);
    strtok(ligne, "\n");
    initLigne = strtol(ligne, NULL, 10);

    //chemin du fichier annuaire
    char *annuaireFileName = concat("storage/", concat(email, "/annuaire"));
    fileAnnuaire = fopen(annuaireFileName, "rw");

    // ouverture du fichier temporaire
    fileAnnuaireTemp = fopen(temp, "w");

    if (fileAnnuaire == NULL || fileAnnuaireTemp == NULL) {
        Emission(RED "500 :" NO_COLOR
                 "Une erreur est survenue (fichier introuvable), réessayer plus tard\n");
        Emission("\r\n");
        return 0;
    }

    // vérification de la correspondance de ligne indiquée avec le nombre de ligne du fichier
    if (getNbLine(fileAnnuaire) < initLigne) {
        Emission(RED "400 :" NO_COLOR "Numéro de ligne saisie inalide\n");
        Emission("\r\n");
        return 0;
    }

    while (!feof(fileAnnuaire)) {
        strcpy(curr, "\0");
        fgets(curr, 1024, fileAnnuaire);
        if (!feof(fileAnnuaire)) {
            countLigne++;
            if (countLigne != initLigne) {
                fprintf(fileAnnuaireTemp, "%s", curr);
            }
        }
    }
    free(curr);

    //fermeture des fichiers et renommage
    fclose(fileAnnuaire);
    fclose(fileAnnuaireTemp);
    rename(temp, annuaireFileName);
    Emission(GREEN "200 : Contact modifié avec succès" NO_COLOR "\n");
    Emission("\r\n");
    return 1;
}
