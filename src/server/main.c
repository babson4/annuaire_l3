#include "serveur.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @author Bastien
 */
int main() {
    char *message = NULL;
    char *typeReq;

    Initialisation();

    while (1) {
        int fini = 0;

        AttenteClient();

        while (!fini) {
            message = Reception();

            //check AUTH
            if (strcmp(typeRequete(message), "AUTH") == 0) {
                fprintf(stdout, CYAN "INFO :" NO_COLOR "Traitement pour AUTH\n\n");
                if (pduAuth(message) == 0){
                    Emission(RED "400 : Le couple <email> <pass> n'existe pas dans nos fichiers"
                             "vous à votre administrateur pour plus d'informations" NO_COLOR
                             "\n");
                    Emission("\r\n");
                    TerminaisonClient();
                    message = NULL; //terminaison client
                } else {
                    Emission(GREEN "200 : OK utilisateur connecté " NO_COLOR "\n");
                    Emission("\r\n");
                }
            }

            // traitement reception message du client
            if (message == NULL) {
                fini = 1;
            } else {
                // debug
                fprintf(stdout, MAGENTA "DEBUG :" NO_COLOR "valeur de message : %s",
                        message);

                typeReq = typeRequete(message);

                // Traitement des PDU
                if (strcmp(typeReq, "GET") == 0) {
                    fprintf(stdout, CYAN "INFO :" NO_COLOR "Traitement pour GET\n\n");

                    pduGet(message);

                } else if (strcmp(typeReq, "SEARCH") == 0) {
                    fprintf(stdout, CYAN "INFO :" NO_COLOR "Traitement pour SEARCH\n\n");

                    pduRecherche(message);

                } else if (strcmp(typeReq, "ADD") == 0) {
                    fprintf(stdout, CYAN "INFO :" NO_COLOR "Traitement pour ADD\n\n");

                    pduAdd(message);

                } else if (strcmp(typeReq, "EDIT") == 0) {
                    fprintf(stdout, CYAN "INFO :" NO_COLOR "Traitement pour EDIT\n\n");

                    pduEdit(message);

                } else if (strcmp(typeReq, "DELETE") == 0) {
                    fprintf(stdout, CYAN "INFO :" NO_COLOR "Traitement pour DELETE\n\n");

                    pduDelete(message);
                } else if (strcmp(typeReq, "GET_RIGHT") == 0) {
                    fprintf(stdout,
                            CYAN "INFO :" NO_COLOR "Traitement pour GET_RIGHT\n\n");
                    pduGetRight(message);
                } else if (strcmp(typeReq, "QUIT") == 0) {
                    fprintf(stdout, CYAN "INFO :" NO_COLOR "Traitement pour QUIT\n\n");
                    // on clot la connexion client
                    Emission("100 QUIT\n");
                    Emission("\r\n");
                    TerminaisonClient(); // fermeture connexion client / serveur
                    fini = 1; // sortie de boucle proprement
                } else if (strcmp(typeReq, "GRANT") == 0) {
                    fprintf(stdout, CYAN "INFO :" NO_COLOR "Traitement pour GRANT\n\n");
                    pduGrant(message);
                } else if (strcmp(typeReq, "ADD_USR") == 0) {
                    //###################################################################
                    //############################# ADMIN ###############################
                    //###################################################################
                    fprintf(stdout, CYAN "INFO :" NO_COLOR "Traitement pour ADD_USR\n\n");
                    pduUserCreate(message);
                } else if (strcmp(typeReq, "DEL_USR") == 0) {
                    fprintf(stdout, CYAN "INFO :" NO_COLOR "Traitement pour DEL_USR\n\n");
                    pduDelUser(message);
                } else if (strcmp(typeReq, "GET_ALL") == 0) {
                    fprintf(stdout, CYAN "INFO :" NO_COLOR "Traitement pour GET_ALL\n\n");
                    pduShowAll();
                }
                free(typeReq);
            }
            free(message);
        }
    }
}
