#ifndef __SERVEUR_H__
#define __SERVEUR_H__
#endif
#define NO_COLOR "\x1b[0m"
#define BLACK "\x1b[30m"
#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define WHITE "\x1b[37m"
#include <stddef.h>
#include <stdio.h>

/* Initialisation.
 * Creation du serveur.
 * renvoie 1 si ea c'est bien passe 0 sinon
 */
int Initialisation();

/* Initialisation.
 * Creation du serveur en precisant le service ou numero de port.
 * renvoie 1 si ea c'est bien passe 0 sinon
 */
int InitialisationAvecService(char *service);

/* Attends qu'un client se connecte.
 * renvoie 1 si ea c'est bien passe 0 sinon
 */
int AttenteClient();

/* Recoit un message envoye par le client.
 * retourne le message ou NULL en cas d'erreur.
 * Note : il faut liberer la memoire apres traitement.
 */
char *Reception();

/* Envoie un message au client.
 * Attention, le message doit etre termine par \n
 * renvoie 1 si ea c'est bien passe 0 sinon
 */
int Emission(char *message);

/* Ferme la connexion avec le client.
 */
void TerminaisonClient();

/* Arrete le serveur.
 */
void Terminaison();

////// fonction ajoutee ////////

/** @author Bastien
 * Fonction qui retourne si le couple email / pass existe
 * 1 en cas d'erreur
 * 0 si tout ok
 */
int isPassValid(char *email, char *pass);

/** @author Bastien
 * Fonction qui verifie si les inforamtion email pass existe et sont valide
 * AUTH email pass
 */
int pduAuth(char *requete);

/** @author Bastien
 * Retourne le type d'action de la PDU requete[1] (GET,ADD,DELETE ...)
 */
char *typeRequete(char *requete);

/** @author Tristan Lopez
 * Afficher un annuaire
 */
int pduGet(char *requete);

/** @author Tristan Lopez
 * Ajouter une ligne dans l'annuaire
 */
int pduAdd(char *requete);

/** @author Tristan Lopez
 *  Supprimer un utilisateur
 */
int pduDelUser(char *requete);

/** @Tristan Vivier
 * fonction pour l'action EDIT
 */
int pduEdit(char *requete);

/** @Tristan Vivier
 * fonction pour l'action GET_RIGHT
 */
int pduGetRight(char *requete);

/** @Tristan Vivier
 * fonction pour l'action GRANT
 */
int pduGrant(char *requete);
/** @author Bastien
 * Fonction qui cherche un paterne dans un fichier d'annuaire et affiche la
 * ligne à l'utilisateur
 */
void pduRecherche(char *requete);
/** @author Emmanuel // Bastien
 * Fonction qui permet de crée un utilisateur pour l'annuaire
 * cree le repertoire de l'utilisateur et tous les fichiers nécessaire pour
 * l'appli.
 */
int pduUserCreate(char *requete);

/** @author Bastien
 * Fonction qui retourne si un utilisateur existe
 */
int userExist(char *email);

/** @author Bastien
 * fonction qui concatère deux chaine
 * concat ne fonctionne pas comme je le veux donc j'ai refait cette fonction
 */
char *concat(const char *str1, const char *str2);

/** @Tristan Vivier
 * fonction qui retourne dans champ le champ N dans la requete
 */
void get_champ_n(char *requete, int num, char *champ);

/** @Tristan Vivier
 * fonction pour l'action GET_RIGHT
 */
int recupereLigne(char *fichier, char *ligne, int numeroLigne);

/** @Tristan Vivier
 * fonction pour l'action GET_RIGHT
 */
int editeLigne(char *ancienneLigne, char *nouvelleLigne, int modif,
               char *nouveauChamp);

// strstr mais non sensible à la casse (délaration de l'entete uniquement)
char *strcasestr(const char *meule_de_foin, const char *aiguille);

/** @author Bastien
 * Fonction qui retourne une chaine contenant le hash MD5 d'une chaine
 */
char *md5Hash(char *string, char *hash);

/// fait par Vivier
/// vérifie que le user qui demande a le droit de consulter l'annuaire demandé
/// retourne 1 si l'utilisateur demandant a le droit; 0 sinon
/// pour rappel la requete sera de la forme GET user_asking user_cible
int userHaveRight(char *requete);

/** @author Bastien
 * Fonction retourne la chaine <string> en majuscule
 */
char *strToUpper(const char *string);

/** @author Bastien
 * Retourne le nombre de ligne d'un fichier
 */
int getNbLine(FILE *file);

/** @author Bastien
 * retourne 0 si l'utilisateur est admin
 */
int isAdmin(char *email);

/** @author Tristan Lopez
 *  Affiche tous les utilisateurs
 */
void pduShowAll();

/** @author Bastien
 * supprime les fichier du dossier "pathDossier"
 *
 */
int delFiles(char *pathDossier);

/** @author Bastien
 * supprime les "/"  de string
 */
void removeSlash(char *string);

//@author Emmanuel
// fonction qui permet de supprimer une ligne d'un annuaire
int pduDelete(char *requete);
