#ifndef __CLIENT_H__
#define __CLIENT_H__
#define NO_COLOR "\x1b[0m"
#define BLACK "\x1b[30m"
#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define WHITE "\x1b[37m"
#include <stddef.h>

#define LONGUEUR_TAMPON 8192

/* Initialisation.
 * Connexion au serveur sur la machine donnee.
 * Utilisez localhost pour un fonctionnement local.
 * renvoie 1 si ea c'est bien passe 0 sinon
 */
int Initialisation(char *machine);

/* Initialisation.
 * Connexion au serveur sur la machine donnee et au service donne.
 * Utilisez localhost pour un fonctionnement local.
 * renvoie 1 si ea c'est bien passe 0 sinon
 */
int InitialisationAvecService(char *machine, char *service);

/* Recoit un message envoye par le serveur.
 * retourne le message ou NULL en cas d'erreur.
 * Note : il faut liberer la memoire apres traitement.
 */
char *Reception();

/* Envoie un message au serveur.
 * Attention, le message doit etre termine par \n
 * renvoie 1 si ea c'est bien passe 0 sinon
 */
int Emission(char *message);

/* Ferme la connexion.
 */
void Terminaison();

//////////// FONCTION AJOUTEE /////////////////:
/** @author Bastien
 * procedure qui affiche le menu d'accueil
 */
void afficherMenuAccueil(char *email);

/** @author Bastien
 * Fonction qui lit le choix utilisateur du menuAccueil
 * et affiche la suite du menu si besoin
 */
void getEtProcessChoixUser(char *userCourant);

/** @author Bastien
 * procedure affiche les sous menu de afficherMenuAccueil
 */
void ecrireSousMenu(int choix, char *email);

/** @author Bastien
 * fonction qui concatère deux chaine
 * concat ne fonctionne pas comme je le veux donc j'ai refait cette fonction
 */
char *concat(const char *str1, const char *str2);

/** @author Bastien
 * Fonction qui cherche un paterne dans un fichier d'annuaire et affiche la
 * ligne à l'utilisateur
 */
void rechercherDansAnnuaire(char *emailAnnuaire, char *paterne);

// strstr mais non sensible à la casse (délaration de l'entete uniquement)
char *strcasestr(const char *meule_de_foin, const char *aiguille);

/** @author Bastien
 * Fonction qui retourne si un utilisateur existe
 */
int userExist(char *email);

/** @author Bastien
 * Fonction qui retourne une chaine contenant le hash MD5 d'une chaine
 */
char *md5Hash(char *string, char *hash);

/** @author Emmanuel // Bastien
 * Fonction qui permet de crée un utilisateur pour l'annuaire
 * cree le repertoire de l'utilisateur et tous les fichiers nécessaire pour
 * l'appli.
 */
int userCreate(char *email, char *pass);

/** @author Bastien
 * retourne 0 si l'utilisateur est admin
 */
int isAdmin(char *email);

/** @author Bastien
 * affiche l'annuaire d'une personne avec les numéros de lignes
 * res contient le retour de pduGet()
 */
void getAnnuaireNumero();
#endif
