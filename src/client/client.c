#include "client.h"
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define TRUE 1
#define FALSE 0
#define MAX 1024

#define LONGUEUR_TAMPON 8192

/* le socket client */
int socketClient;
/* le tampon de reception */
char tamponClient[LONGUEUR_TAMPON];
int debutTampon;
int finTampon;

/* Initialisation.
 * Connexion au serveur sur la machine donnee.
 * Utilisez localhost pour un fonctionnement local.
 */
int Initialisation(char *machine) {
    return InitialisationAvecService(machine, "13214");
}

/* Initialisation.
 * Connexion au serveur sur la machine donnee et au service donne.
 * Utilisez localhost pour un fonctionnement local.
 */
int InitialisationAvecService(char *machine, char *service) {
    int num;
    struct addrinfo hints, *res, *ressave;

    bzero(&hints, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    if ((num = getaddrinfo(machine, service, &hints, &res)) != 0) {
        fprintf(stderr, "Initialisation, erreur de getaddrinfo : %s",
                gai_strerror(num));
        return 0;
    }
    ressave = res;

    do {
        socketClient = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
        if (socketClient < 0)
            continue; /* ignore this one */

        if (connect(socketClient, res->ai_addr, res->ai_addrlen) == 0)
            break; /* success */

        close(socketClient); /* ignore this one */
    } while ((res = res->ai_next) != NULL);

    if (res == NULL) {
        perror("Initialisation, erreur de connect.");
        return 0;
    }

    freeaddrinfo(ressave);
    // debug
    // printf("Connexion avec le serveur reussie.\n");
    return 1;
}

/* Recoit un message envoye par le serveur.
 */
char *Reception() {
    int retour = 0;
    memset(tamponClient, 0, LONGUEUR_TAMPON);
    retour = recv(socketClient, tamponClient, LONGUEUR_TAMPON - 1, 0);
    if (retour < 0) {
        perror("Reception, erreur de recv.");
        return NULL;
    } else if (retour == 0) {
        fprintf(stderr, "Vous avez été déconnecté.\n");
        return NULL;
    }

    // fprintf(stdout, CYAN "Valeur tamponClient :%s" NO_COLOR "\n",
    // tamponClient); fprintf(stdout, RED "FIN" NO_COLOR "\n");

    return strdup(tamponClient);
}

/* Envoie un message au serveur.
 * Attention, le message doit etre termine par \n
 */
int Emission(char *message) {
    if (strstr(message, "\n") == NULL) {
        fprintf(stderr, "Emission, Le message n'est pas termine par \\n.\n");
    }
    int taille = strlen(message);
    if (send(socketClient, message, taille, 0) == -1) {
        perror("Emission, probleme lors du send.");
        return 0;
    }
    // debug
    // printf("Emission de %d caracteres.\n", taille + 1);
    return 1;
}

/* Ferme la connexion.
 */
void Terminaison() { close(socketClient); }

//////////////// FONCTION AJOUTEE //////////////////

/** @author Bastien
 * fonction qui concatère deux chaine
 * strcat ne fonctionne pas comme je le veux donc j'ai refait cette fonction
 */
char *concat(const char *str1, const char *str2) {

    char *result = malloc(strlen(str1) + strlen(str2) + 1); // +1 for the \0
    strcpy(result, str1);
    strcat(result, str2);
    return result;
}

/** @author Bastien
 * retourne 0 si l'utilisateur est admin
 */
int isAdmin(char *email) { return strcmp("admin@email.fr", email) != 0; }

/** @author Bastien
 * procedure qui affiche le menu d'accueil
 */
void afficherMenuAccueil(char *email) {
    fprintf(stdout,
            "Identifiant de l'utilisateur connecté : " WHITE "%s" NO_COLOR "\n",
            email);
    fprintf(stdout, CYAN "\t1." NO_COLOR "Consulter " MAGENTA "son " NO_COLOR
                    "annuaire\n");
    fprintf(stdout, CYAN "\t2." NO_COLOR "Consulter " MAGENTA "un" NO_COLOR
                    " annuaire\n");
    fprintf(stdout,
            CYAN "\t3." NO_COLOR "Rechercher un contact" NO_COLOR "\n");
    fprintf(stdout, CYAN "\t4." NO_COLOR "Ajouter un contact " NO_COLOR "\n");
    fprintf(stdout, CYAN "\t5." NO_COLOR "Modifier un contact\n");
    fprintf(stdout, CYAN "\t6." NO_COLOR "Supprimer un contact\n");
    fprintf(
            stdout, CYAN
                    "\t7." NO_COLOR
                    "Afficher la liste des personnes autorisées à consulter mon annuaire\n");
    fprintf(stdout, CYAN "\t8." RED "Quitter " NO_COLOR "\n");
    fprintf(stdout, CYAN "\t9." NO_COLOR
                    "Autoriser une personne à consulter son annuaire\n");
    if (isAdmin(email) == 0) {
        fprintf(stdout, RED "\t#### ACTION ADMINISTRATEUR ####" NO_COLOR "\n");
        fprintf(stdout, CYAN "\t10." NO_COLOR "Créer un utilisateur\n");
        fprintf(stdout, CYAN "\t11." NO_COLOR "Supprimer un utilisateur\n");
        fprintf(stdout, CYAN "\t12." NO_COLOR "Afficher tous les utilisateurs\n");
    }
}

/** @author Bastien
 * Fonction qui lit le choix utilisateur du menuAccueil
 * et affiche la suite du menu si besoin
 */
void getEtProcessChoixUser(char *email) {
    int choix;
    char *userInput;
    char inputScan[4][MAX];

    fprintf(stdout, "\nSaisir le numéro de l'action voulu :\n\t> ");

    // on recupère le choix du menu
    while (scanf("%d", &choix) != 1) {
        // clear buffer
        while (getchar() != '\n')
            continue;
        fprintf(stdout,
                RED "402 : Choix non valide, saisir le numero de l'action" NO_COLOR
                "\n\t>");
    }
    // clear buffer
    while (getchar() != '\n')
        continue;

    // si user pas admin et choix admin (>9) --> la porte
    // ou choix supp aux nombre possible
    if ((isAdmin(email) == 1 && choix >= 10) || choix > 12 || choix <= 0) {
        fprintf(
                stdout, RED
                        "\n\n402 : Choix non valide, saisir le numero de l'action\n" NO_COLOR
                        "\n\t>");

        afficherMenuAccueil(email);

        // redemander le choix, récurence
        getEtProcessChoixUser(email);
    } else {

        // traitement sous menu
        // 1 et 12 sont sans sous menu
        if (choix != 1 && choix != 12) {
            ecrireSousMenu(choix, email);
        }

        // recup la chaine de requete (userInput)
        switch (choix) {
            case 1:
                // GET email (sans input car son annuaire)
                userInput = concat("GET ", email);
                if (Emission(concat(userInput, "\n")) != 1) {
                    fprintf(stderr,
                            RED "401 : Erreur d'emission (1:GET client)" NO_COLOR "\n");
                }
                break;
            case 2:
                // GET email emailAutre
                // on recupère le choix du menu
                scanf("%s", inputScan[0]);
                userInput = concat(email, concat(" ", concat(inputScan[0], "\n")));
                if (Emission(concat("GET ", userInput)) != 1) {
                    fprintf(stderr,
                            RED "401 : Erreur d'emission (2:GET client)" NO_COLOR "\n");
                }

                break;
            case 3:
                // SEARCH email PATERNE
                scanf("%s", inputScan[0]);
                userInput = concat(email, concat(" ", concat(inputScan[0], "\n")));
                if (Emission(concat("SEARCH ", userInput)) != 1) {
                    fprintf(stderr,
                            RED "401 : Erreur d'emission (SEARCH client)" NO_COLOR "\n");
                }

                break;
            case 4:
                // ADD email NOM PRENOM EMAIL TEL
                scanf("%s %s %s %s", inputScan[0], inputScan[1], inputScan[2],
                      inputScan[3]);
                //mise en forme des arguments utilisateur
                userInput = concat(
                        email,
                        concat(" ",
                               concat(inputScan[0],
                                      concat(" ",
                                             concat(inputScan[1],
                                                    concat(" ",
                                                           concat(inputScan[2],
                                                                  concat(" ",
                                                                         concat(inputScan[3],
                                                                                "\n")))))))));
                if (Emission(concat("ADD ", userInput)) != 1) {
                    fprintf(stderr,
                            RED "401 : Erreur d'emission (ADD client)" NO_COLOR "\n");
                }
                break;
            case 5:
                // EDIT email LIGNE CHAMP VALEUR
                scanf("%s %s %s", inputScan[0], inputScan[1], inputScan[2]);
                userInput = concat(
                        email,
                        concat(" ", concat(inputScan[0],
                                           concat(" ", concat(inputScan[1],
                                                              concat(" ", concat(inputScan[2],
                                                                                 "\n")))))));
                if (Emission(concat("EDIT ", userInput)) != 1) {
                    fprintf(stderr,
                            RED "401 : Erreur d'emission (EDIT client)" NO_COLOR "\n");
                }
                break;
            case 6:
                // DELETE email LIGNE
                scanf("%s", inputScan[0]);
                userInput = concat(email, concat(" ", concat(inputScan[0], "\n")));
                if (Emission(concat("DELETE ", userInput)) != 1) {
                    fprintf(stderr,
                            RED "401 : Erreur d'emission (DELETE client)" NO_COLOR "\n");
                }

                break;
            case 7:
                // GET_RIGHT email emailAutre (emailAutre peux etre égale a email si non
                // admin)
                if (isAdmin(email) == 0) {
                    scanf("%s", inputScan[0]);
                    userInput = concat(email, concat(" ", concat(inputScan[0], "\n")));
                    if (Emission(concat("GET_RIGHT ", userInput)) != 1) {
                        fprintf(stderr, RED
                                        "401 : Erreur emission (GET_RIGHT (admin) client)" NO_COLOR
                                        "\n");
                    }
                } else {
                    if (Emission(concat("GET_RIGHT ", concat(email, "\n"))) != 1) {
                        fprintf(stderr, RED
                                        "401 : Erreur emission (GET_RIGHT (user) client)" NO_COLOR
                                        "\n");
                    }
                }
                break;

            case 8:
                // QUIT(sans input)
                userInput = concat("QUIT ", email);
                if (Emission(concat(userInput, "\n")) != 1) {
                    fprintf(stderr,
                            RED "401 : Erreur d'emission (QUIT client)" NO_COLOR "\n");
                }
                break;
            case 9:
                // GRANT email emailAutre
                scanf("%s", inputScan[0]);
                userInput = concat(email, concat(" ", concat(inputScan[0], "\n")));
                if (Emission(concat("GRANT ", userInput)) != 1) {
                    fprintf(stderr,
                            RED "401 : Erreur d'emission (GRANT client)" NO_COLOR "\n");
                }
                break;
            case 10:
                // ADD_USR email EmailToAdd Pass
                scanf("%s %s", inputScan[0], inputScan[1]);
                userInput = concat(
                        email, concat(" ", concat(inputScan[0],
                                                  concat(" ", concat(inputScan[1], "\n")))));
                if (Emission(concat("ADD_USR ", userInput)) != 1) {
                    fprintf(stderr,
                            RED "401 : Erreur d'emission (ADD_USR client)" NO_COLOR "\n");
                }
                break;
            case 11:
                // DEL_USR email EmailToDel
                scanf("%s", inputScan[0]);
                userInput = concat(email, concat(" ", concat(inputScan[0], "\n")));
                if (Emission(concat("DEL_USR ", userInput)) != 1) {
                    fprintf(stderr,
                            RED "401 : Erreur d'emission (DEL_USR client)" NO_COLOR "\n");
                }
                break;
            case 12:
                // GET_ALL email (sans input)
                userInput = concat("GET_ALL ", email);
                if (Emission(concat(userInput, "\n")) != 1) {
                    fprintf(stderr,
                            RED "401 : Erreur d'emission (GET_ALL client)" NO_COLOR "\n");
                }
                break;
            default:
                fprintf(stdout, "403 : Tu as réussis l'impossible bravo");
                break;
        }
    }
}

/** @author Bastien
 * procedure affiche les sous menu de afficherMenuAccueil
 */
void ecrireSousMenu(int choix, char *email) {
    switch (choix) {
        case 2:
            // get
            fprintf(stdout, "De quel utilisateur voulez vous voir l'annuaire ?\n\t> ");
            break;
        case 3:
            // SEARCH
            fprintf(stdout, "Chercher : ");
            break;
        case 4:
            // ADD
            fprintf(stdout, "Saisir les informations à ajouter sous la forme : <NOM> "
                            "<PRENOM> <EMAIL> <TEL>\n\t> ");
            break;
        case 5:
            // EDIT
            fprintf(stdout, "Votre annuaire va s'afficher pour vous permettre de "
                            "choisir une ligne à editer:\n\n");
            sleep(2);
            // affichage de son annuaire
            if (Emission(concat(concat("GET ", email), "\n")) != 1) {
                fprintf(
                        stderr, RED
                                "401 : Erreur d'emission (ecrireSousMenu case 5 (client))" NO_COLOR
                                "\n");
            }
            getAnnuaireNumero(email);

            fprintf(stdout, "Pour editer une ligne, saisir son numéro, le champ a "
                            "modifier et "
                            "sa valeur: <NUMERO DE LIGNE> <CHAMP> <VALEUR>\n\t> ");
            break;
        case 6:
            // DELETE
            fprintf(stdout, "Votre annuaire va s'afficher pour vous permettre de "
                            "choisir une ligne à supprimer:\n\n");
            sleep(2);

            // affichage de son annuaire
            if (Emission(concat(concat("GET ", email), "\n")) != 1) {
                fprintf(
                        stderr, RED
                                "401 : Erreur d'emission (ecrireSousMenu case 5 (client))" NO_COLOR
                                "\n");
            }
            getAnnuaireNumero(email);

            fprintf(stdout, "Pour supprimer une ligne, saisir son numéro\n\t> ");
            break;
        case 7:
            // GET_RIGHT
            if (isAdmin(email) == 0) {
                fprintf(stdout, "De quel annuaire voulez vous voir les droit ?\n\t> ");
            } else {
                fprintf(stdout, "Affichage...\n");
            }
            break;
        case 8:
            // QUIT
            fprintf(stdout, "deconnexion...\n\n");
            break;
        case 9:
            // GRANT
            fprintf(stdout, "Saisir l'adresse mail de la personne à qui vous voulez "
                            "ajouter les droits de lecture sur votre annuaire\n\t> ");
            break;
        case 10:
            // ADD_USR
            fprintf(stdout, "Saisir l'adresse mail et le mot de passe de la personne à "
                            "ajouter\n\t> ");
            break;
        case 11:
            // DEL_USR
            fprintf(stdout, "Saisir l'adresse mail de l'utilisateur à supprimer\n\t> ");
            break;
        default:
            fprintf(stdout, "403 : Tu as réussis l'impossible bravo");
            break;
    }
}

/** @author Bastien
 * affiche l'annuaire d'une personne avec les numéros de lignes
 * res contient le retour de pduGet()
 */
void getAnnuaireNumero() {
    char *tmp = calloc(LONGUEUR_TAMPON + 1, sizeof(char));
    char delim[] = "\n";
    int fini = 0;

    // on attend de recevoir la requete en entier
    do {
        char *morceauCourant = Reception();
        if (!morceauCourant) {
            break;
        }

        if (strcmp(morceauCourant, "\r\n") == 0) {
            fini = 1;
        } else {
            // formation du message de reception
            tmp = concat(tmp, morceauCourant);
        }

        free(morceauCourant);
    } while (!fini);

    // split de la chaine sur \n
    // affiche en ajoutant les numéros de lignes
    char *ptr = strtok(tmp, delim);
    fprintf(stdout, CYAN "\t  PRENOM\tNOM\tEMAIL\tNUM" NO_COLOR "\n" );
    for (int i = 1; ptr != NULL; i++) {
        fprintf(stdout, CYAN "\t%d." NO_COLOR " %s\n", i, ptr);
        ptr = strtok(NULL, delim);
    }
    fprintf(stdout, "\n");
}
