#include "client.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @author Bastien
 */
int main(int argc, char *argv[]) {
    // controle des arguments
    if (argc < 3) {
        fprintf(stderr,
                RED "Format attendu: bin/clientMain <email> <pass> référez "
                "vous à votre administrateur pour plus d'informations" NO_COLOR
                "\n");
        exit(EXIT_FAILURE);
    }

    // connexion client - serveur
    if (Initialisation("localhost") != 1) {
        fprintf(stdout, CYAN "100 INFO : Erreur d'init, serveur non joignable \n" NO_COLOR);
        return 1;
    }

    // demande au serveur de controler l'identité
    // AUTH email pass
    if (Emission(concat("AUTH ", concat(argv[1], concat(" ", concat(argv[2], "\n"))))) != 1) {
        fprintf(stderr,
                RED "401 : Erreur d'emission (AUTH login)" NO_COLOR "\n");
        return 1;
    }

    // connexion OK, traitement des demandes tant que l'utilisateur ne veux pas quitter
    int userWantToQuit = 0;
    int auth = 0; // variable pour skip l'affichage du menu pour l'authentification
    while (!userWantToQuit) {
        char *message = "";
        int fini = 0;

        // traitement principale
        // demande à l'utilisateur son choix puis execute le traitement assosicé
        // argv[1] est l'utilisateru courant;
        if (auth == 1) {
            afficherMenuAccueil(argv[1]);
            getEtProcessChoixUser(argv[1]);
        }

        do {
            char *morceauCourant = Reception();
            if (morceauCourant == NULL) {
                break;
            }
            // verrif si le client veux quitter (ou erreur de connexion)
            if (strstr(morceauCourant, "QUIT") != NULL) {
                userWantToQuit = 1;
            }

            if (strstr(morceauCourant, "\r\n") != NULL) {
                fini = 1;
            }

            // traitement du message, formation du message de reception
            message = concat(message, morceauCourant);

            free(morceauCourant);
        } while (!fini);

        if (auth == 1) {

            fprintf(stdout, CYAN "\nResultat :" NO_COLOR "\n");
            fprintf(stdout, "%s\n", message);

            fprintf(stdout,
                    "Pour continuer appuyer sur " CYAN "<entrée>" NO_COLOR "\n");
            getchar();
        } else {
            // resultat de l'auth
            fprintf(stdout, "%s\n", message);

            //controle du retour du serveur sur l'auth
            if (strstr(message, "400")) {
                return 0;
            }
        }
        auth = 1; // on rétablie le menu
    }
    return 0;
}
